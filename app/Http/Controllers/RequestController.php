<?php
namespace App\Http\Controllers;

use App\Models\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request as HttpRequest;

/**
 * Контроллер для работы с заявками.
 */
class RequestController extends Controller
{
    /**
     * Добавляет заявку в базу.
     *
     * @param  \Illuminate\Http\Request      $httpRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(HttpRequest $httpRequest): JsonResponse
    {
        $httpRequest->validate([
            'name'       => 'required',
            'email'      => 'required|email:rfc,dns',
            'phone'      => 'required',
            'nomination' => 'required',
        ]);

        $request             = new Request();
        $request->name       = $httpRequest->input('name');
        $request->email      = $httpRequest->input('email');
        $request->phone      = $httpRequest->input('phone');
        $request->nomination = $httpRequest->input('nomination');

        $request->save();

        return response()->json([
            'status'  => 'ok',
            'message' => 'Ваша заявка зарегистрирована.',
        ]);
    }
}
