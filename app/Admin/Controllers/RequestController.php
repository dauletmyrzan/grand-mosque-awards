<?php

namespace App\Admin\Controllers;

use App\Models\Request;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class RequestController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Заявки';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Request());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Имя и фамилия'));
        $grid->column('email', __('Email'));
        $grid->column('phone', __('Телефон'));
        $grid->column('nomination', __('Номинация'));
        $grid->column('email_sent', __('Письмо отправлено'));
        $grid->column('created_at', __('Дата регистрации'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Request::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Имя и фамилия'));
        $show->field('email', __('Email'));
        $show->field('phone', __('Телефон'));
        $show->field('nomination', __('Номинация'));
        $show->field('email_sent', __('Письмо отправлено'));
        $show->field('created_at', __('Дата регистрации'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Request());

        $form->text('name', __('Имя и фамилия'));
        $form->email('email', __('Email'))->readonly();
        $form->mobile('phone', __('Телефон'));
        $form->text('nomination', __('Номинация'));
        $form->switch('email_sent', __('Письмо отправлено'));

        return $form;
    }
}
