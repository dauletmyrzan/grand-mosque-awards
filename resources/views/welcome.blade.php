<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Grand Mosque Architect Awards</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <div class="container-fluid video-container p-3">
        <div id="main" class="d-flex w-100 mx-auto flex-column">
            <video src="/storage/video.mp4" autoplay loop muted></video>
            <header class="p-5">
                <button class="button ripple float-md-end float-none mx-auto d-block">
                    <span class="button-text">Принять участие</span>
                </button>
            </header>
            <section class="heading-section my-auto text-center">
                <h1><span>We build</span> history</h1>
                <div class="arrow-down bounce"></div>
            </section>
        </div>
        <div id="menu" class="shadow">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light py-4">
                    <div class="container-fluid">
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <div class="w-100 d-flex flex-row-reverse" id="navbar-flex">
                                <ul class="navbar-nav mb-2 mb-lg-0">
                                    <li class="nav-item">
                                        <a class="nav-link active" aria-current="page" href="#">Я участвую</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#about">О конкурсе</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#requirements">Технические задания</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#reglament">Регламент</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#genplan">Генплан</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#contacts">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="/js/app.js"></script>
</body>
</html>
